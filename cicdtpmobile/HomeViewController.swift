//
//  HomeViewController.swift
//  cicdtpmobile
//
//  Created by William Lin on 30/01/2023.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func getCocktail(_ sender: Any) {
        CocktailWebService.getItems(){ err, success in
            guard err == nil else {
                print("ERRORp")
                return
            }
            guard (success != nil) else {
                return
            }
            DispatchQueue.main.async {
                print("\n J'ai reussi")
                print(success!)
                
                let nextController = CocktailViewController.newInstance(itemList: success!)
                self.navigationController?.pushViewController(nextController, animated: true)
            }
        }
    }
    

}
