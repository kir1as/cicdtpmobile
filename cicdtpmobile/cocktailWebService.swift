//
//  cocktailWebService.swift
//  cicdtpmobile
//
//  Created by William Lin on 30/01/2023.
//

import Foundation

class CocktailWebService{
    
    class func getItems(completion: @escaping (Error?, [Cocktail]?) -> Void){
        
        let url = "http://localhost:3000/cocktail"
        

        print("\n url === === \(url)")
        
        guard let itemURL = URL(string: url) else{
            print("NOOOO")
            return
        }
        
        var request = URLRequest(url: itemURL)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request) { data, res, err in
            guard err == nil else {
                completion(err, nil)
                print("here")
                return
            }
            guard let d = data else {
//                completion(NSError(domain: "com.kirias.cicdtpmobile", code: 2, userInfo: [
//                    NSLocalizedFailureReasonErrorKey: "No data found"
//                ]), nil)
//                return
                print("no cocktail")
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: d, options: .allowFragments)
                print(json)
                guard let json = json as? [[String: Any]] else {
                    print("no cocktail")
                    return
                }
//                let json = try JSONSerialization.jsonObject(with: data!)
//                                guard let postJSON = json as? [[String: Any]] else {
//                                    completion([], NSError(domain: "com.esgi.Cool", code: 1))
//                                    print(json)
//                                    return
//                 }
                
//                let itemList = try JSONDecoder().decode([Cocktail].self, from: d)
//                print(itemList)
                print(json)
                let posts = json.compactMap { dict in
                    return Cocktail(dict: dict)
                }
                print(posts)
                
                completion(nil, posts)
            } catch let err {
                completion(err, nil)
                print("HRERERERE")
                return
            }
        }
        task.resume()
    }
    
    
    
}
