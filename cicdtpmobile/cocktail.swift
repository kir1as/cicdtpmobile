//
//  cocktail.swift
//  cicdtpmobile
//
//  Created by William Lin on 30/01/2023.
//

import Foundation

class Cocktail{
    let id: Int
    let nom: String
    let prix: Double
    let alcool: Bool
    let ingredients: String
    let descript: String

    init(id: Int, nom: String, prix: Double, alcool: Bool,ingredients: String, descript: String) {
        self.id = id
        self.nom = nom
        self.prix = prix
        self.alcool = alcool
        self.ingredients = ingredients
        self.descript = descript
    }
    
    convenience init?(dict: [String: Any]) {
        guard let id = dict["id"] as? Int,
              let nom = dict["nom"] as? String,
              let prix = dict["prix"] as? Double,
              let alcool = dict["alcool"] as? Bool,
              let ingredients = dict["ingredient"] as? String,
              let descript = dict["description"] as? String else {
            return nil
        }
        
        self.init(id: id, nom: nom, prix: prix, alcool: alcool, ingredients: ingredients, descript: descript)
    }
}

extension Cocktail: CustomStringConvertible{
    var description: String {
        return "id = \(self.id), nom = \(self.nom)"
    }
}

