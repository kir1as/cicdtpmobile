//
//  CocktailViewController.swift
//  cicdtpmobile
//
//  Created by William Lin on 30/01/2023.
//

import UIKit

class CocktailViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    @IBOutlet weak var tv_items: UITableView!
    var itemList : [Cocktail]!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_items.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        cell.textLabel?.text = String(itemList[0].id)
        cell.textLabel?.text = itemList[0].nom
        cell.textLabel?.text = String(itemList[0].prix)
        cell.textLabel?.text = String(itemList[0].alcool)
        cell.textLabel?.text = itemList[0].ingredients
        cell.textLabel?.text = itemList[0].description
        
        return cell
    }
    

    override func viewDidLoad() {
        print(itemList[0].nom)
        super.viewDidLoad()
        self.tv_items.dataSource = self
        self.tv_items.delegate = self
        tv_items.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        // Do any additional setup after loading the view.
    }
    
    public class func newInstance(itemList: [Cocktail]) -> CocktailViewController{
        let srvc = CocktailViewController()
        srvc.itemList = itemList
        return srvc
    }

}
